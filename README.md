# SFDC Case Notifier v1.1.4
###### Instant Desktop Notifications when a Case arrives in your Salesforce.com Org.

![Salesforce Case Notifier.png](https://bitbucket.org/repo/XXXda5j/images/2219777858-Salesforce%20Case%20Notifier.png)

## Introduction
-----------------------
Does your day involve constantly watching and refreshing your Salesforce org for support Cases recently logged by customers? If the answer is yes, then we have just the thing for you. We understand that your customer expects instant gratification and responses to their problems. That's why we wanted to ensure that you never miss a notification from them. We bring you the Salesforce Case Notifier - a Chrome Extension that can make your life a whole lot easier. You can now get instant Desktop Notifications each time a customer raises a Case. The best part is that you don't even have to be logged into your Salesforce org! All you need is a working internet connection and an open browser and you are good to go. So you know what that means - you now have more time for your favorite cat videos!


## Under the Hood
-----------------------
The SFDC Case Notifier has been built as a [**Chrome Extension**](https://developer.chrome.com/extensions). 

### How Does it Work
The Chrome Extension connects to a Salesforce account via [OAuth 2.0](https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/intro_understanding_web_server_oauth_flow.htm). A User can then create **Push Topics** and subscribe to the same. At its heart, the Chrome Extension uses [**Salesforce Streaming API**](https://developer.salesforce.com/docs/atlas.en-us.api_streaming.meta/api_streaming/) to get real-time updates on a Case record. The Streaming API works on the concept of *push-pull* model or *publisher-subscriber* model. 

A Salesforce Admin or a User would create a Push Topic in Salesforce. A Push Topic defines particulars of a possible action/event that can occur on a Case record in Salesforce such as a CRUD, if it's an update then which fields were involved and so on. In other words the Push Topic plays the role of a Publisher. 

When an event/action pertaining to the criteria and specifics of a Push Topic occurs, all the subscribed clients will be notified instantly, one of which will be the Chrome Extension. Upon receiving the event the Chrome Extension would then show a Desktop Notification. It would also display the details of the Case in the extension's popup.

### File Structure
```
+ root
    + assets
        + css
            - hopscotch.min.css
            - options.css
            - popup.css
            - salesforce-lightning-design-system-ltng.min.css
        + fonts
            + webfonts
                - SalesforceSans-BoldItalic.woff
                - SalesforceSans-BoldItalic.woff2
                - SalesforceSans-Italic.woff
                - SalesforceSans-Italic.woff2
                - SalesforceSans-Regular.woff
                - SalesforceSans-Regular.woff2
            - SalesforceSans-BoldItalic.ttf
            - SalesforceSans-Italic.ttf
            - SalesforceSans-Regular.ttf
        + icons
            + action-sprite
                + svg
                    - symbols.svg
            + standard-sprite
                + svg
                    - symbols.svg
            + utility-sprite
                + svg
                    - symbols.svg
        + img
            - case16.png
            - case24.png
            - case48.png
            - case64.png
            - case128.png
            - caseAlert.png
            - connected_app_icon.png
            - error.png
            - large.png
            - marquee.png
            - small.png
            - sprite-green.png
            - sprite-orange.png
            - success.png
        + js
            + third-party
                - angular.min.js
                - cometd.js
                - hopscotch.min.js
                - jquery.cometd.js
                - jquery-1.5.1.js
                - json2.js
                - push.min.js
            - background.js
            - constants.js
            - options.js
            - popup.js
            - tour.js
            - utils.js
        + pages
            - background.html
            - options.html
            - popup.html
    - manifest.json
```

### Components
The Chrome Extension primarily has 3 moving parts - **Options**, **Popup** and **Background** pages. 

#### Options
The Options page allows you to connect to a Salesforce account via OAuth 2.0 and allows the User to create Topics and manage Subscriptions. The OAuth 2.0 is performed using the new [**Chrome Identity API**](https://developer.chrome.com/extensions/identity). The Push Topics are created by specifying a SOQL query which contains the fields to be observed/monitored, the criteria and the action (CRUD).

It would be a challenge for an average User or Admin to create a SOQL from scratch. In order to simplify the process, all the *List Views* defined on the Case object are retrieved via the Salesforce REST API and displayed. The advantage here is that since every List View is powered by a SOQL, the challenging requirement for creating a Push Topic is met by a few clicks. Another advantage is that, Admins or Users can create a new List View in Salesforce, the same will be displayed and Users can subscribe to it, thus making it scalable.

![Options.png](https://bitbucket.org/repo/XXXda5j/images/4290282532-Options.png)

#### Popup
The Popup page displays the list of unread Cases. Both the Popup page and the Options page are developed using [*Angular JS v1.6.4*](https://angularjs.org/) at the front end to ease the maintenance and the development life cycle.

![Popup.png](https://bitbucket.org/repo/XXXda5j/images/379530314-Popup.png)

#### Background
The Background page is responsible for enabling subscription to the Topics as well as constantly monitoring for any new events. A Background Page is one that starts automatically when the Browser starts and it does not have a UI of its own.


## Technologies Used
-----------------------
Here is a sneak peek at the technologies used - 
### Front End
1. Angular JS v1.6.4
2. Lightning Design System v2.2.2
3. Hopscotch for Guided Tours

### Back End
1. Chrome Identity API for Open Authorization 2
2. Chrome Storage API for storing Subscribed Topics, Credentials, Unread Cases
3. Chrome Desktop Notifications API
4. Salesforce REST API for List Views and List View Describe calls
5. Salesforce Streaming API for Push Notifications
6. Salesforce Identity API for User Information

Gone are the times when we used XMLHttpRequests (some people call it XHRs) to perform asynchronous requests (AJAX). In this Chrome Extension, the new [**Fetch API**](https://developer.mozilla.org/en/docs/Web/API/Fetch_API#Browser_compatibility) has been employed throughout in every instance where you would have otherwise ended up using an XHR. The new Fetch API is a tremendous improvement on the XHR API built on top of [*Promises*](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Promise).

## Caveats
-----------------------
Like any other app, this one has restrictions or caveats too. They are -

- Salesforce Account must be **API Enabled**. This means that the Chrome Extension will support only the *Professional Edition* with the API Add-on enabled or the *Enterprise Edition* and above.
- The User logging into Salesforce via the Chrome Extension must -
    * Belong to a Profile that has the **API Enabled** System Permission
    * **Read/Write** Access to the *PushTopic* object
    * Access to **Public List Views**

## Queries/Issues/Support
-----------------------
In case of any queries, please reach out to [Deepak](mailto:deepak@dazeworks.com). 

To make the onboarding easier, the App uses [**Hopscotch**](https://github.com/linkedin/hopscotch) from LinkedIn to present Users with guided tours.

![Guided Tour](https://bitbucket.org/repo/XXXda5j/images/1404892205-Screenshot_1.png)

Also the notion of queries does not boil down to just issues/questions. In case you want to learn how to build Chrome Extensions, don't wait, it's just a ping away!

## Contributions
-----------------------
I would love to see folks contributing to this repo by helping me build extensions for [**Firefox**](https://developer.mozilla.org/en-US/Add-ons/WebExtensions) too. This would be a great chance for you to master JavaScript (*the beauty of this language is really left undiscovered by many of us*) and a couple of other frameworks too!