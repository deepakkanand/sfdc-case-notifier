"use strict";

var Background = (
    function() {
        /////////////////
        //Private Vars //
        /////////////////
        var __self              = this
            ,__tokens           = {}
            ,__topics           = {}
            ,__subscribedTopics = []
            ,__channels         = {}
            ,__credsKey         = chrome.runtime.id + Constants.keySuffixes.creds
            ,__topicsKey        = chrome.runtime.id + Constants.keySuffixes.topics
            ,__casesKey         = chrome.runtime.id + Constants.keySuffixes.cases
            ,__channelsKey      = chrome.runtime.id + Constants.keySuffixes.channels
            ,__manifest         = chrome.runtime.getManifest();

        /**
         * The Redirect URL cannot actually be
         * stored on the Manifest as this might
         * keep changing while we are still in
         * the development life cycle.
         */
        __manifest.oauth2.redirect_uri = chrome.identity.getRedirectURL();

        ///////////////////////////////
        //Helper Functions(Closures) //
        ///////////////////////////////
        
        //////////////////////
        //Fetch API Helpers //
        //////////////////////
        var __generateBody = function( data, type ) {
            var reqBody;

            switch( type ) {
                case Constants.dataTypes.formEncoded :
                    reqBody = new URLSearchParams( data );
                    break;
                default:
                    reqBody = JSON.stringify( data );
                    break
            }             

            return reqBody;
        };

        var __generateHeaders = function( customHeaders ) {
            var headers = new Headers();

            for( var key in customHeaders ) {
                headers.append( key, customHeaders[key] );
            };

            return headers;
        };

        var __generateRequest = function( requestUrl, method, headers, data, dataType ) {
            var options = {
                method  : method,
                headers : headers
            };

            if( data !== null && dataType !== null ) {
                options.body = __generateBody( data, dataType );
            }

            var req = new Request(
                requestUrl,
                options
            );

            return req;
        };

        var __restartBackgroundPage = function() {
            chrome.runtime.getBackgroundPage(
                function( wnd ){
                    console.info( new Date() + " : Background Page Restarted." );

                    wnd.location.reload();
                }
            );
        };

        /**
         * If we have lost Internet Connection
         * or may be say the system has hibernated
         * we will constantly check if the we're
         * back online.
         */
        var __detectOffline = function() {
            console.info( new Date() + " : Timer Set to Detect if Offline." );

            window.setInterval(
                function() {
                    if( !navigator.onLine ) {
                        __restartBackgroundPage();
                    }
                },
                Constants.timeouts.offline
            );
        };

        ////////////////////
        //Case Management //
        ////////////////////
        var __storeUnreadCase = function( unreadCase ) {
            var unreadCases = [];

            if( localStorage[ __casesKey ] ) {
                unreadCases = JSON.parse( localStorage[ __casesKey ] );
            }

            unreadCase.read = false;

            unreadCases.push( unreadCase );

            localStorage[ __casesKey ] = JSON.stringify( unreadCases );
        };

        var __setUnreadCount = function() {
            var unreadCases = [];

            if( localStorage[ __casesKey ] ) {
                unreadCases = JSON.parse( localStorage[ __casesKey ] );
            }

            chrome.browserAction.setBadgeText(
                {
                    text : unreadCases.length > 100 ? "100+" : unreadCases.length.toString()
                }
            );
        };

        var __showUnreadCount = function() {
            var unreadCases = [];

            if( localStorage[ __casesKey ] ) {
                unreadCases = JSON.parse( localStorage[ __casesKey ] );
            }

            chrome.browserAction.setBadgeText(
                {
                    text : unreadCases.length > 100 ? "100+" : unreadCases.length.toString()
                }
            );
        };

        var __markAsRead = function( readCase ) {
            var unreadCases = [];

            if( localStorage[ __casesKey ] ) {
                unreadCases = JSON.parse( localStorage[ __casesKey ] );
            }
            
            var found = false;
            for( var idx = 0; idx < unreadCases.length; idx++ ) {
                if( unreadCases[idx].Id === readCase.Id ) {
                    found = true;
                    break;
                }
            }
            
            if( found ) {
                /**
                 * Remove the Case that has been
                 * maked as "read" from the list
                 * as well as Local Storage.
                 */
                unreadCases.splice( idx, 1 );

                localStorage[ __casesKey ] = JSON.stringify( unreadCases );

                __setUnreadCount();
            }
        };

        ////////////////////////////
        //Background Page Helpers //
        ////////////////////////////
        var __fetchCredentials = function() {
            __tokens[ __credsKey ] = {};
            chrome.storage.local.get(
                __tokens,
                function( tokens ) {
                    if( tokens[ __credsKey ].refresh_token ) {
                        console.info( new Date() + " : Tokens Read." );

                        __tokens = tokens[ __credsKey ];

                        __refreshTokens();
                    }
                }
            );
        };

        var __refreshTokens = function() {
            var req = __generateRequest(
                __manifest.oauth2.token,
                Constants.methods.post,
                __generateHeaders(
                    {
                        "Content-Type"  : Constants.dataTypes.formEncoded,
                        "Accept"        : Constants.dataTypes.applicationJSON,
                    }
                ),
                __manifest.oauth2.refresh_token.format(
                    __manifest.oauth2.client_id,
                    __manifest.oauth2.client_secret,
                    __tokens.refresh_token
                ),
                Constants.dataTypes.formEncoded
            );

            fetch( req ).then(
                function( response ) {
                    return response.json();
                }
            ).then(
                function( data ) {
                    console.info( new Date() + " : Access Token Refreshed." );

                    __tokens.access_token = data.access_token;
                    
                    var creds = {};
                    creds[ __credsKey ] = __tokens;

                    chrome.storage.local.set(
                        creds,
                        function() {
                            __fetchSubscribedTopics();
                        }
                    );
                }
            ).catch(
                function( err ) {
                    __restartBackgroundPage();
                }
            );
        };

        var __fetchSubscribedTopics = function() {
            __topics[ __topicsKey ] = [];
            chrome.storage.local.get(
                __topics,
                function( topics ) {
                    if( topics[ __topicsKey ] &&
                        topics[ __topicsKey ].length > 0
                    ) {
                        __subscribedTopics = topics[ __topicsKey ];
                        
                        __initCometD();
                    }
                }
            );
        };

        /**
         * Subscribe to the Streaming API
         * using CometD.
         */
        var __subscribeToTopics = function() {
            __subscribedTopics.forEach(
                function( topicId ) {
                    $.cometd.subscribe(
                        Constants.urls.topic.format( topicId ),
                        function( message ) {
                            __storeUnreadCase( message.data.sobject );

                            __showUnreadCount();
                            
                            Push.create( Constants.notificationHeaders.caseAlert, {
                                body    : Constants.messages.caseAlert.format(
                                    message.data.event.type,
                                    message.data.sobject.CaseNumber
                                ),
                                icon    : Constants.urls.caseAlertImg,
                                timeout : Constants.timeouts.alert,
                                onClick : function () {
                                    /**
                                     * Mark a Case as read when the
                                     * User clicks on the Notification.
                                     */
                                    __markAsRead( message.data.sobject );

                                    window.open(
                                        Constants.urls.openInSF.format(
                                            __tokens.instance_url,
                                            __tokens.access_token,
                                            message.data.sobject.Id
                                        ),
                                        "_blank"
                                    );
                                }
                            } );
                        }
                    );
                }
            );
            
            console.info( new Date() + " : Subscribed to Topics." );
        };

        ///////////////////////////
        //Binding Event Handlers //
        ///////////////////////////
        var __bindEvents = function() {
            chrome.runtime.onInstalled.addListener(
                function( details ) {
                    chrome.storage.local.clear(
                        function() {
                            localStorage.clear();

                            console.info( new Date() + " : Cleared Storage." );

                            var error = chrome.runtime.lastError;
                            if ( error ) {
                                console.error( error );
                            }
                        }
                    );
                }
            );
            
            chrome.storage.onChanged.addListener(
                function( changes, areaName ) {
                    if( changes[ __topicsKey ] ) {
                        __restartBackgroundPage();
                    }
                }
            );

            /**
             * Set a timer for refresh
             * after 'x' milliseconds.
             * This is a precaution in
             * place to prevent the Access
             * Tokens from getting invalidated
             * and so on.
             */
            /**
             * Commenting this out for now.
             * I think we will have to refresh
             * only when the CometD is trying
             * disconnect.
             */
            /*            
            console.info( new Date() + " : Timer Set for Refresh." );
            window.setTimeout(
                function() {
                    __restartBackgroundPage();
                },
                Constants.timeouts.bckPgRefresh
            );
            */
        };

        var __initCometD = function() {
            console.info( new Date() + " : CometD Init Started." );

            $.cometd.init(
                {
                    url                     : Constants.urls.cometD.format( __tokens.instance_url ),
                    requestHeaders          : {
                        Authorization       : Constants.prefixes.oauth + __tokens.access_token
                    },
                    appendMessageTypeToURL  : false
                }
            );


            /**
             * Do the Subscription only after
             * the "Handshake" was successful.
             * Please note that the code in the
             * Salesforce Developer Docs is
             * incorrect as they are attempting
             * a "subscribe" without listening
             * to the "meta/handshake" and confirming
             * if it was successful or not.
             */
            $.cometd.addListener(
                "/meta/handshake",
                function( message ) {
                    if( message.successful ) {
                        console.info( new Date() + " : CometD Handshake Completed." );

                        __subscribeToTopics();
                    }
                    else {
                        /**
                         * If the Handshake failed we
                         * need to restart the Background
                         * Page to re-initiate the Sub-
                         * scription process.
                         */
                        console.info( new Date() + " : CometD Handshake Failed." );
                        console.info( new Date() + " : Restarting Background Page." );

                        __restartBackgroundPage();
                    }
                }
            );

            /**
             * If it's getting disconnected
             * we should restart the Background
             * page in order to re-initiate
             * the connection.
             */
            $.cometd.addListener(
                "/meta/connect",
                function( message ) {
                    if( $.cometd.isDisconnected() ) {
                        console.info( new Date() + " : CometD Disconnected." );
                        console.info( new Date() + " : Restarting Background Page." );

                        __restartBackgroundPage();

                    }
                }
            );

            /**
             * Restart the Background Page
             * if the CometD has disconnected.
             */
            $.cometd.addListener(
                "/meta/disconnect",
                function( message ) {
                    console.info( new Date() + " : CometD Disconnected." );
                    console.info( new Date() + " : Restarting Background Page." );

                    __restartBackgroundPage();
                }
            );
        };

        ////////////////
        //Initializer //
        ////////////////
        var __init = function() {
            __detectOffline();

            __bindEvents();

            __fetchCredentials();

            __showUnreadCount();
        };

        __init();
    }
)();