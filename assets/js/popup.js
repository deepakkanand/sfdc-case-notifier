"use strict";

angular.module( "popupApp" , [] ).controller(
    "PopupController",
    function( $scope ) {
        //////////
        //Model //
        //////////
        this.model = {
            loggedIn            : false,
            subscribedTopics    : [],
            unreadCases         : []
        };

        //////////////////////
        //Private Variables //
        //////////////////////
        var __self          = this
            ,__credsKey     = chrome.runtime.id + Constants.keySuffixes.creds
            ,__casesKey     = chrome.runtime.id + Constants.keySuffixes.cases
            ,__topicsKey    = chrome.runtime.id + Constants.keySuffixes.topics;

        /////////////////////
        //Helper Functions //
        /////////////////////
        var __fetchSubscribedTopics = function() {
            var subscribedTopics = {};
            subscribedTopics[ __topicsKey ] = [];

            chrome.storage.local.get(
                subscribedTopics,
                function( subscribedTopics ) {
                    __self.model.subscribedTopics = subscribedTopics[ __topicsKey ];

                    $scope.$apply();
                }
            );
        };

        var __fetchUnreadCases = function() {
            var unreadCases = [];

            if( localStorage[ __casesKey ] ) {
                JSON.parse( localStorage[ __casesKey ] ).forEach(
                    function( unreadCase ) {
                        if( !unreadCase.read ) {
                            __self.model.unreadCases.push( unreadCase );
                        }
                    }
                );
            }
        };

        var __setUnreadCount = function() {
            var unreadCases = [];

            if( localStorage[ __casesKey ] ) {
                unreadCases = JSON.parse( localStorage[ __casesKey ] );
            }

            chrome.browserAction.setBadgeText(
                {
                    text : unreadCases.length > 100 ? "100+" : unreadCases.length.toString()
                }
            );
        };

        ////////////////
        //Initializer //
        ////////////////
        var __init = function( tokens) {
            __self.model.tokens     = tokens;

            __self.model.loggedIn   = true;

            __fetchSubscribedTopics();

            __fetchUnreadCases();
        };

        ///////////////////
        //Event Handlers //
        ///////////////////
        this.handlers = {
            detectLoggedIn : function() {                
                var creds = {};
                creds[ __credsKey ] = {};

                chrome.storage.local.get(
                    creds,
                    function( creds ) {
                        var tokens = creds[ __credsKey ];

                        if( tokens.refresh_token ) {
                            __init( tokens );

                            $scope.$apply();
                        }
                    }
                );
            },
            markAsRead : function( readCase ) {
                var found = false;
                for( var idx = 0; idx < __self.model.unreadCases.length; idx++ ) {
                    if( __self.model.unreadCases[idx].Id === readCase.Id ) {
                        found = true;
                        break;
                    }
                }
                
                if( found ) {
                    /**
                     * Remove the Case that has been
                     * maked as "read" from the list
                     * on the Model as well as the 
                     * Local Storage.
                     */
                    __self.model.unreadCases.splice( idx, 1 );

                    localStorage[ __casesKey ] = JSON.stringify( __self.model.unreadCases );

                    __setUnreadCount();

                    __self.handlers.openInSalesforce( readCase );
                }
            },
            markAllRead : function() {
                __self.model.unreadCases = [];
                localStorage[ __casesKey ] = [];

                __setUnreadCount();
            },
            openInSalesforce : function( unreadCase ) {
                __self.handlers.markAsRead( unreadCase );
                
                /**
                 * Open the Case in Salesforce
                 * via the frontdoor.jsp
                 * Refer - https://help.salesforce.com/articleView?id=000193348&type=1
                 * to know how the frontdoor.jsp
                 * works.
                 */
                window.open(
                    Constants.urls.openInSF.format(
                        __self.model.tokens.instance_url,
                        __self.model.tokens.access_token,
                        unreadCase.Id
                    ),
                    "_blank"
                );
            }
        };
    }
);