"use strict";

var tour = {
    id : "sfdc-case-notifier",
    steps : [
        {
            title : "Logging Out",
            content : "You can use this button to Log Out from Salesforce at any time. Please note that once you Log Out from Salesforce, you will no longer receive notifications.",
            target : "btnLogout",
            placement : "left"
        },

        {
            title : "Options Tab",
            content : "This tab lets you configure the extension. You can create new Topics, delete existing Topics as well as manage your Topic Subscriptions.",
            target : "liOptions",
            placement : "bottom"
        },

        {
            title : "Help Tab",
            content : "Are you stuck?!? Or maybe you have an error that has popped up? Then this is the Tab you should look for.",
            target : "liHelp",
            placement : "bottom"
        },

        {
            title : "Creating Topics",
            content : "A Topic lets you describe a change that might happen to a Case record and refers to any such change that you would like to be notified about. A change could be that a  Case was Closed, or a new Case was created or may be a change to one or more fields on the Case. The Topic is defined by means of a SOQL (Salesforce Object Query Language) query.",
            target : "sectionPushTopics",
            placement : "bottom"
        },

        {
            title : "Selecting a List View",
            content : "Not everyone would know SOQL queries and that shouldn't stop you from creating a Topic. Thus to make things easy, you can choose one of the List Views defined on the Case object to start creating the Topic. Whenever there is change on the selected List View, say a new record pertaining to the criteria of the List View was created, you would be notified instantly. Wait! Isn't the List View that you are looking for there? All you have to do is, go to Salesforce, create the List View, refresh the Options page and you should see it here.",
            target : "selListView",
            placement : "left"
        },

        {
            title : "Confirm Your SOQL",
            content : "Yes, every List View has a Query (SOQL) behind it. Well, its really not much to be concerned about. We are just showing you the query in case you are the curious kind :-)",
            target : "txtSOQL",
            placement : "left"
        },

        {
            title : "Save the Topic",
            content : "You have selected the List View and also had a sneak peek at the query. Now why wait? Go ahead and click the Create button to create the Topic.",
            target : "btnCreate",
            placement : "right"
        },

        {
            title : "Available Topics",
            content : "Here you will see the Topic(s) that you have created. You may also see Topics created by your colleagues. In other words, all the Topics available in your Salesforce Org that you can subscribe to!",
            target : "sectionAvailableTopics",
            placement : "top"
        },

        {
            title : "Topic Names",
            content : "This column would show you the name of the Topic(s). If the Topic was created by the Extension, you will see the text \"Created by Salesforce Case Notifier v1.0....\". This is because, when we create a Topic from the Extension, this Description is added so that  Users can differentiate between Topic(s) created from the extension and elsewhere.",
            target : "divTopics",
            placement : "top"
        },

        {
            title : "Topic Actions",
            content : "And yeah this is what you have longed for! You can Subscribe to a Topic and that's when the game changes. After you subscribe, you can safely close this tab and you will start receiving instant Desktop Notifications each time a case is created. You can also unsubscribe or delete a Topic.",
            target : "divActions",
            placement : "left"
        }
    ],
    onEnd : function() {
        tour.setTourCompletionStatus( true );
    },
    setTourCompletionStatus : function( flag ) {
        var __tourKey = chrome.runtime.id + Constants.keySuffixes.tour;
        
        var tourCompleted           = {};
        tourCompleted[ __tourKey ]  = flag;

        chrome.storage.local.set(
            tourCompleted,
            function() {

            }
        );
    }
};