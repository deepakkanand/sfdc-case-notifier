"use strict";

var Constants = (
    function() {
        return {
            emptyString : "",
            apiVersion  : 23.0,
            notificationHeaders : {
                success     : "Success",
                error       : "Error",
                caseAlert   : "Case Alert"
            },
            messages : {
                loginSuccess    : "You have logged into Salesforce successfully.",
                pushTopicDesc   : "Created by Salesforce Case Notifier v1.0 for Case List View - {0}",
                pushTopicSuccess: "Created Push Topic for List View - {0} Successfully.",
                pushTopicSubs   : "Subscribed to Push Topic for List View - {0} Successfully.",
                pushTopicUnSub  : "Un-subscribed from Push Topic for List View - {0} Successfully.",
                caseAlert       : "A Case with number - {1} has been {0}."
            },
            errorCodes : {
                "CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY" : "Your Salesforce Account does not have full access to the PushTopic object. Please contact your System Administrator.",
                "INVALID_TYPE" : "Your Salesforce Account does not have full access to the PushTopic object. Please contact your System Administrator.",
                "INSUFFICIENT_ACCESS_OR_READONLY" : "Your Salesforce Account does not have full access to the PushTopic object. Please contact your System Administrator.",
                "NOT_FOUND" : "Your Salesforce Account does not have full access to the PushTopic object. Please contact your System Administrator."
            },
            dataTypes : {
                formEncoded     : "application/x-www-form-urlencoded",
                applicationJSON : "application/json"
            },
            charsets : {
                utf8 : "; charset=UTF-8"
            },
            methods : {
                post    : "POST",
                get     : "GET",
                delete  : "DELETE"
            },
            keySuffixes : {
                creds   : "__creds",
                topics  : "__topics",
                cases   : "__cases",
                channels: "__channels",
                tour    : "__tour"
            },
            prefixes : {
                bearer : "Bearer ",
                oauth  : "OAuth "
            },
            urls : {
                successImg      : "../img/success.png",
                errorImg        : "../img/error.png",
                caseAlertImg    : "../img/caseAlert.png",
                listViews       : "{0}/services/data/v39.0/sobjects/Case/listviews",
                listViewDesc    : "{0}/services/data/v39.0/sobjects/Case/listviews/{1}/describe",
                pushTopicCreate : "{0}/services/data/v39.0/sobjects/PushTopic",
                pushTopicDel    : "{0}/services/data/v39.0/sobjects/PushTopic/{1}",
                allPushTopics   : "{0}/services/data/v39.0/query?q=SELECT+Id,+Name,+Description+FROM+PushTopic",
                cometD          : "{0}/cometd/23.0/",
                topic           : "/topic/{0}",
                openInSF        : "{0}/secur/frontdoor.jsp?sid={1}&retURL=/{2}",
                identity        : "?oauth_token={0}&format=json"
            },
            urlParams : {
                authCode : "code"
            },
            timeouts : {
                error       : 5000,
                alert       : 5000,
                success     : 3000,
                bckPgRefresh: 900000,
                offline     : 1000
            }
        }
    }
)();