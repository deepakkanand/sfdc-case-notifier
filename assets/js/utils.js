"use strict";

if( !String.prototype.format ) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace( /{(\d+)}/g, function( match, number ) {
            return typeof args[number] != 'undefined' ?
                args[number] :
                match;
        } );
    };
}

if( !Array.prototype.pushUnique ) {
    Array.prototype.pushUnique = function( item ) {
        if( this.indexOf( item ) === -1 ) {
            this.push( item );
        }
    };
}

if( !Array.prototype.remove ) {
    Array.prototype.remove = function() {
        var what
            ,a = arguments
            ,L = a.length
            ,ax;

        while( L && this.length ) {
            what = a[--L];
            while( ( ax = this.indexOf( what ) ) !== -1 ) {
                this.splice( ax, 1 );
            }
        }

        return this;
    };
}