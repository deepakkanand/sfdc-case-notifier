"use strict";

angular.module( "optionsApp", [] ).controller(
    "OptionsController",
    function( $scope ) {
        //////////
        //Model //
        //////////
        this.model = {
            loggingIn           : false,
            loggedIn            : false,
            activeTab           : 0,
            busy                : false,

            //Inputs
            selectedListView    : Constants.emptyString,
            soqlForPushTopic    : Constants.emptyString
        };

        //////////////////////
        //Private Variables //
        //////////////////////
        var  __self      = this
            ,__manifest  = chrome.runtime.getManifest()
            ,__credsKey  = chrome.runtime.id + Constants.keySuffixes.creds
            ,__topicsKey = chrome.runtime.id + Constants.keySuffixes.topics;


        /**
         * The Redirect URL cannot actually be
         * stored on the Manifest as this might
         * keep changing while we are still in
         * the development life cycle.
         */
        __manifest.oauth2.redirect_uri = chrome.identity.getRedirectURL();

        /////////////////////
        //Helper Functions //
        /////////////////////
        var __generateBody = function( data, type ) {
            var reqBody;

            switch( type ) {
                case Constants.dataTypes.formEncoded :
                    reqBody = new URLSearchParams( data );
                    break;
                default:
                    reqBody = JSON.stringify( data );
                    break
            }             

            return reqBody;
        };

        var __generateHeaders = function( customHeaders ) {
            var headers = new Headers();

            for( var key in customHeaders ) {
                headers.append( key, customHeaders[key] );
            };

            return headers;
        };

        var __generateRequest = function( requestUrl, method, headers, data, dataType ) {
            var options = {
                method  : method
            };

            if( headers !== null ) {
                options.headers = headers;
            }

            if( data !== null && dataType !== null ) {
                options.body = __generateBody( data, dataType );
            }

            var req = new Request(
                requestUrl,
                options
            );

            return req;
        };

        var __toggleBusyFlag = function( isBusy ) {
            __self.model.busy = isBusy;
        };

        var __logginIn = function( isLoggingIn ) {
            __self.model.loggingIn = isLoggingIn;
        };

        var __restartBackgroundPage = function() {
            chrome.runtime.getBackgroundPage(
                function( wnd ){
                    console.info( new Date() + " : Background Page Restarted." );

                    wnd.location.reload();
                }
            );
        };

        /**
         * Use the Salesforce Identity API to fetch
         * details of the User such as the 
         * Display Name.
         */
        var __fetchUserDetails = function( tokens ) {
            var req = __generateRequest(
                tokens.id + Constants.urls.identity.format( tokens.access_token ),
                Constants.methods.get,
                null,
                null,
                null
            );

            fetch( req ).then(
                function( response ) {
                    return response.json();
                }
            ).then(
                function( data ) {
                    console.info( new Date() + " : User Details Fetched." );

                    var creds = {};
                    creds[ __credsKey ] = tokens;
                    creds[ __credsKey ].display_name = data.display_name;

                    chrome.storage.local.set(
                        creds,
                        function() {                            
                            __restartBackgroundPage();

                            Push.create( Constants.notificationHeaders.success, {
                                body    : Constants.messages.loginSuccess,
                                icon    : Constants.urls.successImg,
                                timeout : Constants.timeouts.success,
                                onClick : function () {
                                    this.close();
                                }
                            } );

                            __toggleBusyFlag( false );

                            $scope.$apply();

                            /**
                             * Begin fetching List Views
                             * and Available Topics.
                             */
                            __init( creds[ __credsKey ] );
                        }
                    );
                }
            ).catch(
                function( err ) {
                    Push.create( Constants.notificationHeaders.error, {
                        body    : err.message,
                        icon    : Constants.urls.errorImg,
                        timeout : Constants.timeouts.error,
                        onClick : function () {
                            this.close();
                        }
                    } );

                    __toggleBusyFlag( false );

                    $scope.$apply();
                }
            );
        };

        ///////////////////
        //Token Requests //
        ///////////////////
        var __getRefreshToken = function( authCode ) {
            var req = __generateRequest(
                __manifest.oauth2.token,
                Constants.methods.post,
                __generateHeaders(
                    {
                        "Content-Type"  : Constants.dataTypes.formEncoded,
                        "Accept"        : Constants.dataTypes.applicationJSON,
                    }
                ),
                __manifest.oauth2.access_token.format(
                    __manifest.oauth2.client_id,
                    __manifest.oauth2.client_secret,
                    __manifest.oauth2.redirect_uri,
                    authCode
                ),
                Constants.dataTypes.formEncoded
            );

            fetch( req ).then(
                function( response ) {
                    return response.json();
                }
            ).then(
                function( data ) {
                    console.info( new Date() + " : Refresh Token Fetched." );

                    var creds = {};
                    creds[ __credsKey ] = data;

                    chrome.storage.local.set(
                        creds,
                        function() {
                            __fetchUserDetails( data );
                        }
                    );
                }
            ).catch(
                function( err ) {
                    Push.create( Constants.notificationHeaders.error, {
                        body    : err.message,
                        icon    : Constants.urls.errorImg,
                        timeout : Constants.timeouts.error,
                        onClick : function () {
                            this.close();
                        }
                    } );

                    __toggleBusyFlag( false );

                    $scope.$apply();
                }
            );
        };

        var __getAccessToken = function() {
            __toggleBusyFlag( true );

            var req = __generateRequest(
                __manifest.oauth2.token,
                Constants.methods.post,
                __generateHeaders(
                    {
                        "Content-Type"  : Constants.dataTypes.formEncoded,
                        "Accept"        : Constants.dataTypes.applicationJSON,
                    }
                ),
                __manifest.oauth2.refresh_token.format(
                    __manifest.oauth2.client_id,
                    __manifest.oauth2.client_secret,
                    __self.model.tokens.refresh_token
                ),
                Constants.dataTypes.formEncoded
            );

            fetch( req ).then(
                function( response ) {
                    return response.json();
                }
            ).then(
                function( data ) {
                    console.info( new Date() + " : Access Token Refreshed." );

                    __self.model.tokens.access_token = data.access_token;

                    var creds = {};
                    creds[ __credsKey ] = __self.model.tokens;

                    chrome.storage.local.set(
                        creds,
                        function() {
                            __restartBackgroundPage();

                            __toggleBusyFlag( false );

                            $scope.$apply();
                            
                            /**
                             * Don't send the "data" as
                             * that only has the "access_token".
                             * If you do so, you will loose
                             * the "refreh_token". Just send
                             * the "tokens" on the Model
                             * itself.
                             */
                            __init( __self.model.tokens );
                        }
                    );
                }
            ).catch(
                function( err ) {
                    Push.create( Constants.notificationHeaders.error, {
                        body    : err.message,
                        icon    : Constants.urls.errorImg,
                        timeout : Constants.timeouts.error,
                        onClick : function () {
                            this.close();
                        }
                    } );

                    __toggleBusyFlag( false );

                    $scope.$apply();
                }
            );
        };

        /////////////////////
        //Tour Initializer //
        /////////////////////
        var __startTour = function() {
            var __tourKey = chrome.runtime.id + Constants.keySuffixes.tour;
            
            var tourCompleted           = {};
            tourCompleted[ __tourKey ]  = false;

            chrome.storage.local.get(
                tourCompleted,
                function( tourCompleted ) {
                    if( !tourCompleted[ __tourKey ] ) {
                        hopscotch.startTour( tour );
                    }
                }
            );
        };

        ///////////////
        //List Views //
        ///////////////
        
        /**
         * The SOQL query behind a List View
         * might have ORDER BY clauses or
         * fields from related object.This is not
         * supported in a PushTopic hence
         * normalizing it by removing them.
         */
        var __normalizeSOQL = function( data ) {
            var fieldsToRemove      = [];
            var soqlForPushTopic    = data.query;

            data.columns.forEach(
                function( column ) {
                    if( column.fieldNameOrPath.indexOf( "." ) > -1 ) {
                        soqlForPushTopic = soqlForPushTopic.replace( column.fieldNameOrPath + ",", Constants.emptyString );
                    }
                }
            );

            if( data.orderBy && data.orderBy.length > 0 ) {
                soqlForPushTopic = soqlForPushTopic.split( "ORDER BY" )[0];
            }

            return soqlForPushTopic;
        };

        /**
         * Build a JS Object where the
         * Property will be the List View
         * Id while the Value will be the
         * List View details itself.
         */
        var __hashListViews = function( listViews ) {
            __self.model.__listViewsHash = {};

            __self.model.listViews.forEach(
                function( listView ) {
                    __self.model.__listViewsHash[ listView.id ] = listView;
                }
            );
        };

        var __fetchListViews = function() {
            __toggleBusyFlag( true );

            var req = __generateRequest(
                Constants.urls.listViews.format(
                    __self.model.tokens.instance_url
                ),
                Constants.methods.get,
                __generateHeaders(
                    {
                        "Content-Type"  : Constants.dataTypes.applicationJSON + Constants.charsets.utf8,
                        "Accept"        : Constants.dataTypes.applicationJSON,
                        "Authorization" : Constants.prefixes.bearer + __self.model.tokens.access_token
                    }
                ),
                null,
                null
            );

            fetch( req ).then(
                function( response ) {
                    if( response.status === 401 ) {                        
                        __toggleBusyFlag( false );

                        $scope.$apply();

                        __getAccessToken();
                    }
                    else {
                        return response.json();
                    }
                }
            ).then(
                function( data ) {
                    __toggleBusyFlag( false );

                    $scope.$apply();

                    if( data && data[0] && data[0].errorCode ) {
                        Push.create( Constants.notificationHeaders.error, {
                            body    : data[0].message,
                            icon    : Constants.urls.errorImg,
                            timeout : Constants.timeouts.error,
                            onClick : function () {
                                this.close();
                            }
                        } );
                    }
                    else if( data && data.listviews ) {
                        __self.model.listViews = data.listviews;

                        __hashListViews();                       

                        __fetchAvailableTopics();
                    }
                }
            ).catch(
                function( err ) {
                    Push.create( Constants.notificationHeaders.error, {
                        body    : err.message,
                        icon    : Constants.urls.errorImg,
                        timeout : Constants.timeouts.error,
                        onClick : function () {
                            this.close();
                        }
                    } );

                    __toggleBusyFlag( false );

                    $scope.$apply();
                }
            );
        };

        /////////////////////
        //Available Topics //
        /////////////////////
        var __fetchAvailableTopics = function() {
            __toggleBusyFlag( true );

            var req = __generateRequest(
                Constants.urls.allPushTopics.format( __self.model.tokens.instance_url ),
                Constants.methods.get,
                __generateHeaders(
                    {
                        "Content-Type"  : Constants.dataTypes.applicationJSON + Constants.charsets.utf8,
                        "Accept"        : Constants.dataTypes.applicationJSON,
                        "Authorization" : Constants.prefixes.bearer + __self.model.tokens.access_token
                    }
                ),
                null,
                null
            );

            fetch( req ).then(
                function( response ) {
                    return response.json();
                }
            ).then(
                function( data ) {
                    __toggleBusyFlag( false );

                    if( data && data[0] && data[0].errorCode ) {
                        Push.create( Constants.notificationHeaders.error, {
                            body    : Constants.errorCodes[data[0].errorCode] || data[0].message,
                            icon    : Constants.urls.errorImg,
                            timeout : Constants.timeouts.error,
                            onClick : function () {
                                this.close();
                            }
                        } );

                        $scope.$apply();
                    }
                    else {
                        __self.model.pushTopics = data.records;

                        var subcribedTopics = {};
                        subcribedTopics[ __topicsKey ] = [];

                        chrome.storage.local.get(
                            subcribedTopics,
                            function( subcribedTopics ) {
                                __self.model.pushTopics.forEach(
                                    function( topic ) {
                                        if( subcribedTopics[ __topicsKey ].indexOf( topic.Name ) !== -1 ) {
                                            topic.subscribed = true;
                                        }
                                        else {
                                            topic.subscribed = false;
                                        }
                                    }
                                );

                                __startTour();

                                $scope.$apply();
                            }
                        );
                    }
                }
            ).catch(
                function( err ) {
                    Push.create( Constants.notificationHeaders.error, {
                        body    : err.message,
                        icon    : Constants.urls.errorImg,
                        timeout : Constants.timeouts.error,
                        onClick : function () {
                            this.close();
                        }
                    } );

                    __toggleBusyFlag( false );

                    $scope.$apply();
                }
            );
        };

        ////////////////
        //Initializer //
        ////////////////
        var __init = function( tokens ) {
            __self.model.loggedIn   = true;
            __self.model.tokens     = tokens;

            __fetchListViews();
        };

        ///////////////////
        //Event Handlers //
        ///////////////////
        this.handlers = {
            loginToSalesforce : function() {
                __logginIn( true );

                __toggleBusyFlag( true );

                var auth_url = __manifest.oauth2.authorize.format(
                    __manifest.oauth2.client_id,
                    __manifest.oauth2.redirect_uri
                );

                /**
                 * Use the Chrome Identity API
                 * to initiate the OAuth flow.
                 */
                chrome.identity.launchWebAuthFlow(
                    {
                        url         : auth_url,
                        interactive : true
                    },
                    function( responseUrl ) {
                        if( responseUrl ) {
                            __toggleBusyFlag( false );

                            $scope.$apply();

                            var authCode = new URL( responseUrl ).searchParams.get( Constants.urlParams.authCode );

                            __getRefreshToken( authCode );
                        }

                        var error = chrome.runtime.lastError;
                        if ( error ) {
                            __toggleBusyFlag( false );
                            
                            __logginIn( false );
                            
                            $scope.$apply();

                            Push.create( Constants.notificationHeaders.error, {
                                body    : error.message,
                                icon    : Constants.urls.errorImg,
                                timeout : Constants.timeouts.error,
                                onClick : function () {
                                    this.close();
                                }
                            } );
                        }
                    }
                );
            },
            logoutFromSalesforce : function() {
                /**
                 * Only remove the "Credentials"
                 * and the "Subscribed Topics" from
                 * the Local Storage. If there is
                 * a Key/Value generated for the Tour,
                 * leave that in tact as we do not want
                 * users to be presented with the Tour
                 * again(if completed) when they log 
                 * back in.
                 */
                chrome.storage.local.remove(
                    [ __credsKey, __topicsKey ],
                    function() {
                        localStorage.clear();

                        console.info( new Date() + " : Cleared Storage." );

                        var error = chrome.runtime.lastError;
                        if ( error ) {
                            console.error( error );
                        }

                        location.reload();
                    }
                );
            },
            detectLoggedIn : function() {
                var creds = {};
                creds[ __credsKey ] = {};

                chrome.storage.local.get(
                    creds,
                    function( creds ) {
                        var tokens = creds[ __credsKey ];

                        if( tokens.refresh_token ) {
                            __init( tokens );
                        }

                        $scope.$apply();
                    }
                );
            },
            /**
             * Invoke the List View Describe API
             * call and fetch the SOQL powering
             * the List View.
             */
            displaySOQL : function() {
                if( __self.model.selectedListView !== Constants.emptyString ) {
                    __toggleBusyFlag( true );

                    var req = __generateRequest(
                        Constants.urls.listViewDesc.format(
                            __self.model.tokens.instance_url,
                            __self.model.selectedListView
                        ),
                        Constants.methods.get,
                        __generateHeaders(
                            {
                                "Content-Type"  : Constants.dataTypes.applicationJSON + Constants.charsets.utf8,
                                "Accept"        : Constants.dataTypes.applicationJSON,
                                "Authorization" : Constants.prefixes.bearer + __self.model.tokens.access_token
                            }
                        ),
                        null,
                        null
                    );

                    fetch( req ).then(
                        function( response ) {
                            return response.json();
                        }
                    ).then(
                        function( data ) {
                            __toggleBusyFlag( false );

                            if( data && data[0] && data[0].errorCode ) {
                                Push.create( Constants.notificationHeaders.error, {
                                    body    : data[0].message,
                                    icon    : Constants.urls.errorImg,
                                    timeout : Constants.timeouts.error,
                                    onClick : function () {
                                        this.close();
                                    }
                                } );

                                $scope.$apply();
                            }
                            else {
                                __self.model.soqlForPushTopic = __normalizeSOQL( data );

                                $scope.$apply();
                            }
                        }
                    ).catch(
                        function( err ) {
                            Push.create( Constants.notificationHeaders.error, {
                                body    : err.message,
                                icon    : Constants.urls.errorImg,
                                timeout : Constants.timeouts.error,
                                onClick : function () {
                                    this.close();
                                }
                            } );

                            __toggleBusyFlag( false );

                            $scope.$apply();
                        }
                    );
                }
                else {
                    __self.model.soqlForPushTopic = Constants.emptyString;
                }
            },
            createPushTopic : function() {
                __toggleBusyFlag( true );

                var req = __generateRequest(
                    Constants.urls.pushTopicCreate.format( __self.model.tokens.instance_url ),
                    Constants.methods.post,
                    __generateHeaders(
                        {
                            "Content-Type"  : Constants.dataTypes.applicationJSON + Constants.charsets.utf8,
                            "Accept"        : Constants.dataTypes.applicationJSON,
                            "Authorization" : Constants.prefixes.bearer + __self.model.tokens.access_token
                        }
                    ),
                    {
                        ApiVersion  : Constants.apiVersion,
                        Description : Constants.messages.pushTopicDesc.format( 
                            __self.model.__listViewsHash[ __self.model.selectedListView ].label
                        ),
                        IsActive    : true,
                        Name        : __self.model.selectedListView,
                        Query       : __self.model.soqlForPushTopic
                    },
                    Constants.dataTypes.applicationJSON
                );

                fetch( req ).then(
                    function( response ) {
                        return response.json();
                    }
                ).then(
                    function( data ) {
                        __toggleBusyFlag( false );

                        if( data && !data.success && data[0].errorCode ) {
                            Push.create( Constants.notificationHeaders.error, {
                                body    : Constants.errorCodes[data[0].errorCode] || data[0].message,
                                icon    : Constants.urls.errorImg,
                                timeout : Constants.timeouts.error,
                                onClick : function () {
                                    this.close();
                                }
                            } );

                            $scope.$apply();
                        }
                        else {
                            Push.create( Constants.notificationHeaders.success, {
                                body    : Constants.messages.pushTopicSuccess.format(
                                    __self.model.__listViewsHash[ __self.model.selectedListView ].label
                                ),
                                icon    : Constants.urls.successImg,
                                timeout : Constants.timeouts.success,
                                onClick : function () {
                                    this.close();
                                }
                            } );

                            $scope.$apply();

                            __fetchAvailableTopics();
                        }
                    }
                ).catch(
                    function( err ) {
                        Push.create( Constants.notificationHeaders.error, {
                            body    : err.message,
                            icon    : Constants.urls.errorImg,
                            timeout : Constants.timeouts.error,
                            onClick : function () {
                                this.close();
                            }
                        } );

                        __toggleBusyFlag( false );

                        $scope.$apply();
                    }
                );
            },
            subscribeToPushTopic : function( topic ) {
                var subcribedTopics = {};
                subcribedTopics[ __topicsKey ] = [];

                chrome.storage.local.get(
                    subcribedTopics,
                    function( subcribedTopics ) {
                        subcribedTopics[ __topicsKey ].pushUnique( topic.Name );

                        topic.subscribed = true;

                        chrome.storage.local.set(
                            subcribedTopics,
                            function() {
                                Push.create( Constants.notificationHeaders.success, {
                                    body    : Constants.messages.pushTopicSubs.format( topic.Description ),
                                    icon    : Constants.urls.successImg,
                                    timeout : Constants.timeouts.success,
                                    onClick : function () {
                                        this.close();
                                    }
                                } );
                            }
                        );

                        $scope.$apply();
                    }
                );
            },
            unsubscribeFromPushTopic : function( topic ) {
                var subcribedTopics = {};
                subcribedTopics[ __topicsKey ] = [];

                chrome.storage.local.get(
                    subcribedTopics,
                    function( subcribedTopics ) {
                        subcribedTopics[ __topicsKey ].remove( topic.Name );

                        topic.subscribed = false;

                        chrome.storage.local.set(
                            subcribedTopics,
                            function() {
                                Push.create( Constants.notificationHeaders.success, {
                                    body    : Constants.messages.pushTopicUnSub.format( topic.Description ),
                                    icon    : Constants.urls.successImg,
                                    timeout : Constants.timeouts.success,
                                    onClick : function () {
                                        this.close();
                                    }
                                } );
                            }
                        );

                        $scope.$apply();
                    }
                );
            },
            /**
             * Remove the Push Topic record
             * from Salesforce.
             */
            deletePushTopic : function( topic ) {
                __toggleBusyFlag( true );

                var subcribedTopics = {};
                subcribedTopics[ __topicsKey ] = [];

                chrome.storage.local.get(
                    subcribedTopics,
                    function( subcribedTopics ) {
                        subcribedTopics[ __topicsKey ] = subcribedTopics[ __topicsKey ].remove( topic.Name );

                        chrome.storage.local.set(
                            subcribedTopics,
                            function() {
                                var req = __generateRequest(
                                    Constants.urls.pushTopicDel.format( __self.model.tokens.instance_url, topic.Id ),
                                    Constants.methods.delete,
                                    __generateHeaders(
                                        {
                                            "Content-Type"  : Constants.dataTypes.applicationJSON + Constants.charsets.utf8,
                                            "Accept"        : Constants.dataTypes.applicationJSON,
                                            "Authorization" : Constants.prefixes.bearer + __self.model.tokens.access_token
                                        }
                                    ),
                                    null,
                                    null
                                );

                                fetch( req ).then(
                                    function( response ) {
                                        if( response.status === 204 ) {
                                            for( var idx = 0; idx < __self.model.pushTopics.length; idx++ ) {
                                                if( __self.model.pushTopics[idx].Id === topic.Id ) {
                                                    __self.model.pushTopics.splice( idx, 1 );
                                                    break;
                                                }
                                            }

                                            __toggleBusyFlag( false );

                                            $scope.$apply();
                                        }
                                        else {
                                            return response.json();
                                        }
                                    }
                                ).then(
                                    function( data ) {
                                        if( data ) {
                                            __toggleBusyFlag( false );

                                            $scope.$apply();

                                            if( data && !data.success && data[0].errorCode ) {
                                                Push.create( Constants.notificationHeaders.error, {
                                                    body    : Constants.errorCodes[data[0].errorCode] || data[0].message,
                                                    icon    : Constants.urls.errorImg,
                                                    timeout : Constants.timeouts.error,
                                                    onClick : function () {
                                                        this.close();
                                                    }
                                                } );
                                            }
                                        }
                                    }
                                ).catch(
                                    function( err ) {
                                        Push.create( Constants.notificationHeaders.error, {
                                            body    : err.message,
                                            icon    : Constants.urls.errorImg,
                                            timeout : Constants.timeouts.error,
                                            onClick : function () {
                                                this.close();
                                            }
                                        } );
                                        
                                        __toggleBusyFlag( false );

                                        $scope.$apply();
                                    }
                                );
                            }
                        );
                    }
                );
            },
            /**
             * Restart a completed tour.
             */
            restartTour : function() {
                tour.setTourCompletionStatus( false );
                location.reload();
            }
        };
    }
);